<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.wowonder.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com
// +------------------------------------------------------------------------+
// | WoWonder - The Ultimate PHP Social Networking Platform
// | Copyright (c) 2017 WoWonder. All rights reserved.
// +------------------------------------------------------------------------+

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';






// Import the Secret Manager client library.
use Google\Cloud\SecretManager\V1\SecretManagerServiceClient;

/** Uncomment and populate these variables in your code */
$projectId = 'mybridgeproduction';
$secretId1 = 'my-secretvideodn3';
$versionId1 = 'latest';
$secretId2 = 'my-secretun2';
$versionId2 = 'latest';
$secretId3 = 'my-secretup4';
$versionId3 = 'latest';
$secretId4 = 'my-secreth1';
$versionId4 = 'latest';




// Create the Secret Manager client.
$client = new SecretManagerServiceClient();

// Build the resource name of the secret version.
$name1 = $client->secretVersionName($projectId, $secretId1, $versionId1);

// Access the secret version.
$response1 = $client->accessSecretVersion($name1);

$sql_db_name = $response1->getPayload()->getData();

// Build the resource name of the secret version.
$name2 = $client->secretVersionName($projectId, $secretId2, $versionId2);

// Access the secret version.
$response2 = $client->accessSecretVersion($name2);

$sql_db_user = $response2->getPayload()->getData();

// Build the resource name of the secret version.
$name3 = $client->secretVersionName($projectId, $secretId3, $versionId3);

// Access the secret version.
$response3 = $client->accessSecretVersion($name3);

$sql_db_pass = $response3->getPayload()->getData();

// Build the resource name of the secret version.
$name4 = $client->secretVersionName($projectId, $secretId4, $versionId4);

// Access the secret version.
$response4 = $client->accessSecretVersion($name4);

$sql_db_host = $response4->getPayload()->getData();


// Site URL
$site_url = "https://mybridge.video"; // e.g (http://example.com)
?>
